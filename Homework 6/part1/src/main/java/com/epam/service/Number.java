package com.epam.service;

import com.epam.annotation.Logged;
import org.springframework.stereotype.Service;


@Service
public class Number {

    @Logged
    public int findNumber(int numb) {
        for (int i = 2; i < numb + 1; i = i * i) {
            if (i > 100000) {
                return numb;
            }else if (i > numb) {
                return i;
            }
        }
        return 5 * 7;
    }
}
